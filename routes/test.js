exports.index = function(req, res) {
	var fs = require('fs');
	fs.readFile(__dirname + '/../public/test.html',
	function (err, data) {
		if (err) {
			res.writeHead(500);
			return res.end('Error loading test.html');
		}
		res.writeHead(200);
		res.end(data);
	});
}