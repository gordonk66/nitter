exports.posted = function(req, res) {
	var name = req.body.name;
	
	if (name) {
		nitter.users[name] = true;
		res.send({
			success: true,
			message: 'successfully logged in'
		});	
	} else {
		res.send({
			success: false,
			message: 'failed to login'
		});
	}

}
