var express = require('express'),
	home = require('./routes/home.js'),
	test = require('./routes/test.js'),
	login = require('./routes/login.js'),
	logout = require('./routes/logout.js'),
	list = require('./routes/list.js'),
	latest = require('./routes/latest.js'),
	message = require('./routes/message.js');
	app = express(),
	nitter = {},
	nitter.users = {}, 
	nitter.messages = [],
	fs = require('fs');

 app.listen(process.env.PORT || 3000);
// app.listen(3000);
console.log(process.env.PORT);

app.use(express.bodyParser());

nitter.io = require('socket.io').listen(3001);
nitter.io.configure(function () {
	nitter.io.set("transports", ["xhr-polling"]);
	nitter.io.set("polling duration", 10);
});

// link up to our public files
app.use("/styles", express.static(__dirname + '/public/styles'));
app.use("/scripts", express.static(__dirname + '/public/scripts'));


// Pages
app.get('/', home.index);
app.get('/test', test.index);

// Services
app.post('/login', login.posted);
app.post('/logout', logout.posted);
app.post('/message', message.posted);
app.get('/latest', latest.index);
app.get('/list', list.index);

		nitter.io.sockets.on('connection', function (socket) {
			nitter.socket = socket; 

		});

// function handler (req, res) {
//   fs.readFile(__dirname + '/index.html',
//   function (err, data) {
//     if (err) {
//       res.writeHead(500);
//       return res.end('Error loading index.html');
//     }

//     res.writeHead(200);
//     res.end(data);
//   });
// }


