Ext.onReady(function(){
	
    // second tabs built from JS
    var tabs2 = Ext.widget('tabpanel', {
        renderTo: document.body,
        activeTab: 0,
        width: '100%',
        height: '100%',
        plain: true,
        defaults:{
            autoScroll: true,
            bodyPadding: 10
        },
        items: [{
                title: 'Login',
				xtype: 'panel',
				layout: 'vbox',
				
				items: [{
					xtype: 'textfield',
					name: 'name',
					id: 'nameTextField',
					fieldLabel: 'Name',
					allowBlank: false
				}, {
					xtype: 'button',
					text: 'Login',
					handler: function() {
						Ext.Ajax.request({
						    url: 'login',
							method: 'post',
						    params: {
						        name: Ext.getCmp('nameTextField').getValue()
						    },
						    success: function(response){
								console.log('success');
								console.debug(response);
						    }
						});
					}
				},
				{
					xtype: 'panel',
					id: 'loginResults',
					html: ''
				}
				]
                
            },	{
		                title: 'Logout',
						xtype: 'panel',
						layout: 'vbox',

						items: [{
							xtype: 'textfield',
							name: 'name',
							id: 'nameTextField',
							fieldLabel: 'Name',
							allowBlank: false
						}, {
							xtype: 'button',
							text: 'Login',
							handler: function() {
								Ext.Ajax.request({
								    url: 'login',
									method: 'post',
								    params: {
								        name: Ext.getCmp('nameTextField').getValue()
								    },
								    success: function(response){
										console.log('success');
										console.debug(response);
								    }
								});
							}
						},
						{
							xtype: 'panel',
							id: 'loginResults',
							html: ''
						}
						]

		           }
        ]
    });
});