Ext.onReady(function () {
	// second tabs built from JS
	var tabs2 = Ext.widget('panel', {
		layout: 'hbox',
		renderTo: document.body,
		activeTab: 0,
		width: '100%',
		height: '100%',
		plain: true,
		defaults: {
			autoScroll: true,
			bodyPadding: 10
		},
		items: [
			{
				xtype: 'panel',
				id: 'loginContainer',
				layout: 'vbox',
				width: 200,
				items: [
					{
						title: 'Login',
						xtype: 'panel',
						layout: 'vbox',
						width: 200,
						height: 200,

						items: [
							{
								xtype: 'textfield',
								name: 'name',
								id: 'nameTextField',
								fieldLabel: 'Name',
								allowBlank: false
							},
							{
								xtype: 'button',
								text: 'Login',
								handler: function () {
									Ext.Ajax.request(
									{
										url: 'login',
										method: 'post',
										params: {
											name: Ext.getCmp('nameTextField').getValue()
										},
										success: function (response) {
											console.log('success');
											username = Ext.getCmp('nameTextField').getValue();

										}
									});
								}
							},
							{
								xtype: 'button',
								text: 'Refresh',
								handler: function () {
									Ext.Ajax.request(
									{
										url: 'list',
										method: 'get',
										params: {
											name: Ext.getCmp('nameTextField').getValue()
										},
										success: function (response) {
											response = Ext.decode(response.responseText);
											console.log('success');
											var junk= "";
											Ext.Object.each(response.users, function(key) {
												junk = junk + key + '<br>';
											}, this);
											Ext.getCmp('usersPanel').update(junk);
											

										}
									});
								}
							},
							{
								xtype: 'panel',
								id: 'usersPanel',
								height: 200,
								border: 0,
								flex: 1,
								html: ''
							}
						]
					},
					{
						xtype: 'panel',
						height: 300,
						width: '300%',
						items: [
						{
							xtype: 'textfield',
							name: 'newMsg',
							id: 'newMsg',
							fieldLabel: 'Send Msg',
							allowBlank: false
						},
						{
							xtype: 'button',
							text: 'Send',
							handler: function() {
								var newmsg = Ext.getCmp('newMsg').getValue();
								Ext.Ajax.request(
								{
									url: 'message',
									method: 'POST',
									params: {
										name: username,
										message: newmsg
									},
									success: function (response) {
										Ext.getCmp('newMsg').setValue('');
										

									}
								});
							}
						}
						]
					}
					
				]
			},
			{
				title: 'Messages',
        id: 'messages',
				flex: 1,
				height: '100%',
				xtype: 'panel',
				layout: 'vbox',
				items: [
				]

			}
		]
	});
})
;